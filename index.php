<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");
    echo "Name      : ".$sheep->nama ."</br>";
    echo "Legs      : ".$sheep->kaki ."</br>";
    echo "Cold Blood: ".$sheep->cold_blood ."</br><br>";

    $katak = new Frog("Buduk");
    echo "Nama      : ".$katak->nama."</br>";
    echo "Legs      : ".$katak->kaki ."</br>";
    echo "Cold Blood: ".$katak->cold_blood ."</br>";
    echo $katak->jump()."<br><br>";

    $sungokong = new Ape("Kera sakti");
    echo "Nama      : ".$sungokong->nama."</br>";
    echo "Legs      : ".$sungokong->kaki ."</br>";
    echo "Cold Blood: ".$sungokong->cold_blood ."</br>";
    echo $sungokong->yell();
?>